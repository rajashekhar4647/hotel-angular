import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app.router.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';

import { DoctorlayoutComponent } from './layout/doctor/doctorlayout.component';
import { AdminlayoutComponent } from "./layout/admin/adminlayout.component";
import { DefaultlayoutComponent } from "./layout/default/defaultlayout.component";


import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';

import { JwtInterceptor} from './../app/_helpers/jwt.interceptors';
import { AdminService } from "./admin/admin.service";
import { LayoutService } from "./layout/layout.service";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AuthGuard } from "./_guards/auth.guard.service";
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from 'ng2-ckeditor';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    DoctorlayoutComponent,
    AdminlayoutComponent,
    DefaultlayoutComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    MaterialModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NgSelectModule,
    CKEditorModule,
    NgxSpinnerModule,    
    AppRoutingModule // Should be last import
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    AdminService,
    LayoutService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [  
    AppComponent
  ]
})
export class AppModule { }
