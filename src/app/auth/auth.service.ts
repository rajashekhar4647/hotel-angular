import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { environment} from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()
export class AuthService {
    url: string = environment.api.base + environment.api.endPoints.login;

    // httpOptions  = httpOptions;
    constructor(private httpClient: HttpClient) { 
        
    }

    login(userData): Observable<any> {
        return this.httpClient.post(this.url, userData, httpOptions);
    }

    register(userData): Observable<any> {
        return this.httpClient.post(this.url, userData, httpOptions);
    }

    isAuthenticated () {
        
    }

    
}