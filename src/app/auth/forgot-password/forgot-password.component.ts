import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'forgot-password',
    templateUrl: 'forgot-password.component.html'
})

export class ForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;

    constructor(private fb: FormBuilder) {
        this.forgotPasswordForm = fb.group({
            email: ['', Validators.required]
        });
    }

    ngOnInit() { }
}