import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from "../auth.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    userData = {};
    id:number;
    error:string;
    token : any;

    constructor(private fb: FormBuilder,private authService:AuthService, private route: ActivatedRoute,
        private router: Router) {
        this.loginForm = fb.group({
            email: ['', Validators.required],
            
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    ngOnInit() { }

    login(){
        //console.log(this.userData);
        
        this.authService.login(this.userData).subscribe(
            data => {
                    // console.log(data);
                    if(data['status'] == 412){
                        this.error = "Password Mismatch";
                    }
                    else if(data['status'] == 410){
                        this.error = "Username Doesn't Exist";
                    }
                    else{
                       sessionStorage.setItem('hospitalUserToken',data['hospitalUserToken']);
                       sessionStorage.setItem('hospitalUserId',data['hospitalUserId']);
                       sessionStorage.setItem('hospitalUserType',data['hospitalUserType']);
                       sessionStorage.setItem('hospitalUserName',data['hospitalUserName']);
                       sessionStorage.setItem('hospitalUserRefId',data['hospitalUserRefId']);

                        this.router.navigate(['/admin']);
                       
                    }
                    
        }, error => {
            console.log(error);
        });
    }
}