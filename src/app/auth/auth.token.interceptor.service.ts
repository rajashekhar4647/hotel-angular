import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable()
export class AuthTokenInterceptorService implements HttpInterceptor {
    constructor(public authService: AuthService) { }
    
    intercept(request: HttpRequest<any>, next: HttpHandler) {
    
        
    return next.handle(request);
  }
}