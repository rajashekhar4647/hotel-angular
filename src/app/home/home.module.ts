import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material/material.module';

import { HomeRoutingModule } from './home.router.module';

import { HomeComponent } from './home/home.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        HomeRoutingModule
    ],
    exports: [],
    declarations: [
        HomeComponent
    ],
    providers: [],
})
export class HomeModule { }
