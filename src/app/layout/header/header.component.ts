import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.css']
})

export class HeaderComponent implements OnInit {
    userName;
    constructor(
        private route: ActivatedRoute,
        private router:Router,
    ) { }

    ngOnInit() { 
       this.userName = sessionStorage.getItem('hospitalUserName');
    }
    logout(){
        sessionStorage.clear();
        this.router.navigate(['/login']);

    }
}