import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'doctor-layout',
    templateUrl: 'doctorlayout.component.html'
})

export class DoctorlayoutComponent  implements OnInit {
  patientId;
  constructor() { }
  ngOnInit() { 
    this.patientId = sessionStorage.getItem('patientId');
    
    if(this.patientId){
      this.patientId = true;
    }
    else{
      this.patientId = false;
    }
  }
}