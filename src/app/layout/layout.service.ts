import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class LayoutService {
    url: string = environment.api.base + environment.api.endPoints.menus;
    // url1: string = environment.api.base + environment.api.endPoints.roleModule;
    // url2: string = environment.api.base + environment.api.endPoints.rolemenu1;
    
    constructor(private httpClient: HttpClient) { 
     
    }
    
    getMenus(link) {
        return this.httpClient.get(this.url+'/'+link,httpOptions);
    }
    // getModules(token) {
    //     return this.httpClient.get(this.url1+'/'+token,httpOptions);
    // }
    // getMenus1(link) {
    //     return this.httpClient.get(this.url2+'/'+link,httpOptions);
    // }
}