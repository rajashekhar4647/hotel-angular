import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";

@Component({
    selector: 'admin-layout',
    templateUrl: 'adminlayout.component.html'
})

export class AdminlayoutComponent  implements OnInit {

  menuList = [];
    token : string;
    module = 'admin';
    link: string;
  constructor(private layoutService:LayoutService) { }
  ngOnInit() { 
    this.token = sessionStorage.getItem('hospitalUserToken');
    this.link = this.module +'/'+ this.token;
    this.layoutService.getMenus(this.link).subscribe(
        data => {
            this.menuList = data['result']['data'];
            // console.log(this.menuList);
        }, error => {
            console.log(error);
        });
  }
  makeActive(id){
    for(var i=0;i<this.menuList.length;i++){
          this.menuList[i]['active'] = '';
    }  
    for(var i=0;i<this.menuList.length;i++){
          if(id == this.menuList[i]['menuName']){
            //   console.log(this.menuList['menuName']);
            this.menuList[i]['active'] = 'active';
            return;
        }
          else{
            this.menuList[i]['active'] = '';
          }
      }
      console.log(this.menuList);
    //   console.log(id);
  }
}