import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { AdminlayoutComponent } from './layout/admin/adminlayout.component';
import { DefaultlayoutComponent } from "./layout/default/defaultlayout.component";
import { AuthGuard } from "./_guards/auth.guard.service";

const routes: Routes = [
  
    { path: 'auth',component:DefaultlayoutComponent, loadChildren: 'app/auth/auth.module#AuthModule',canActivate : [AuthGuard] },
    { path: 'admin',component:AdminlayoutComponent,loadChildren:'app/admin/admin.module#AdminModule',canActivate : [AuthGuard] },
    
    { path: '', component:DefaultlayoutComponent, loadChildren: 'app/auth/auth.module#AuthModule' },
    { path: '**', component: PageNotFoundComponent }
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
