import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'staff',
    templateUrl: 'staff.component.html'
})

export class StaffComponent implements OnInit {
    staffList =  [];
    staffData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getStaffs().subscribe(
            data => {
                // console.log(data);
                this.staffList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}