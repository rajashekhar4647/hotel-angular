import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'StaffFormComponent',
    templateUrl: 'staffform.component.html'
})

export class StaffFormComponent implements OnInit {
    staffList = [];
    doctorList = [];
    hospitalList = [];
    staffData = {};
    id: number;


    constructor(

        private adminService: AdminService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
       
        if (this.id > 0) {
            $(".fg-line").addClass('fg-toggled');
            this.adminService.getStaffDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.staffData = data['result'];


                }, error => {
                    console.log(error);
                });
        }


        //console.log(this.id);
    }
    addStaff() {
        //    console.log(this.patientData);
        if (this.id > 0) {
            this.adminService.updateStaffItems(this.staffData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/staff']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.adminService.insertStaffItems(this.staffData).subscribe(
                data => {
                    this.router.navigate(['admin/staff']);
                }, error => {
                    console.log(error);
                });
        }

    }
}