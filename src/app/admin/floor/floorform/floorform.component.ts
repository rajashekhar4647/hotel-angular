import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'FloorFormComponent',
    templateUrl: 'floorform.component.html'
})

export class FloorFormComponent implements OnInit {
    floorList = [];
    floorData = {};
    id: number;


    constructor(

        private adminService: AdminService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getFloorDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.floorData['floorName'] = data['result']['floorName'];
                    console.log(this.floorData);

                }, error => {
                    console.log(error);
                });
        }


        //console.log(this.id);
    }
    addFloor() {
        //    console.log(this.patientData);
        if (this.id > 0) {
            this.adminService.updateFloorItems(this.floorData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/floor']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.adminService.insertFloorItems(this.floorData).subscribe(
                data => {
                    this.floorList = data['todos'];
                    this.router.navigate(['admin/floor']);
                }, error => {
                    console.log(error);
                });
        }

    }
}