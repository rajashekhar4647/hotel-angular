import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'floor',
    templateUrl: 'floor.component.html'
})

export class FloorComponent implements OnInit {
    floorList =  [];
    floorData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getFloors().subscribe(
            data => {
                // console.log(data);
                this.floorList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addRoomType(){
           //console.log(this.accountData);
        this.adminService.insertFloorItems(this.floorData).subscribe(
            data => {
                this.floorList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}