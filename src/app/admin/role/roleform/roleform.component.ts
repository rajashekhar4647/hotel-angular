import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'RoleFormComponent',
    templateUrl: 'roleform.component.html'
})

export class RoleFormComponent implements OnInit {
    roleform: FormGroup;
    roleList = [];
    roleData = {};
    roleidparentList = [];
    id: number;
    constructor(

        private AdminService: AdminService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        if (this.id > 0) {
            $(".fg-line").addClass('fg-toggled');
            this.AdminService.getRoleDetail(this.id).subscribe(
                data => {
                    this.roleData = data['result'];

                }, error => {
                    console.log(error);
                });
        }
    }
    addRole() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AdminService.updateRoleItems(this.roleData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/role']);

                }, error => {
                    console.log(error);
                });
        } else {
            this.AdminService.insertRoleItems(this.roleData).subscribe(
                data => {
                    this.router.navigate(['admin/role']);

                }, error => {
                    console.log(error);
                });

        }


    }
}