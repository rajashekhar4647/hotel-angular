import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../admin.service'


@Component({
    selector: 'role',
    templateUrl: 'role.component.html'
})

export class RoleComponent implements OnInit {
    roleList = [];

    constructor(
        
        private AdminService: AdminService
    ) { }

    ngOnInit() { 
        
        this.AdminService.getRoles().subscribe(
            data => {
                this.roleList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}