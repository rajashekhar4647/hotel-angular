import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'RoomTypeFormComponent',
    templateUrl: 'roomtypeform.component.html'
})

export class RoomTypeFormComponent implements OnInit {
    roomtypeList = [];
    roomtypeData = {};
    id: number;


    constructor(

        private adminService: AdminService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getRoomTypeDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.roomtypeData['roomtypeName'] = data['result']['roomtypeName'];
                    console.log(this.roomtypeData);

                }, error => {
                    console.log(error);
                });
        }


        //console.log(this.id);
    }
    addRoomType() {
        //    console.log(this.patientData);
        if (this.id > 0) {
            this.adminService.updateRoomTypeItems(this.roomtypeData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/roomtype']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.adminService.insertRoomTypeItems(this.roomtypeData).subscribe(
                data => {
                    this.roomtypeList = data['todos'];
                    this.router.navigate(['admin/roomtype']);
                }, error => {
                    console.log(error);
                });
        }

    }
}