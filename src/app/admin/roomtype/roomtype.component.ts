import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'roomtype',
    templateUrl: 'roomtype.component.html'
})

export class RoomTypeComponent implements OnInit {
    roomtypeList =  [];
    roomtypeData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getRoomTypes().subscribe(
            data => {
                // console.log(data);
                this.roomtypeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addRoomType(){
           //console.log(this.accountData);
        this.adminService.insertRoomTypeItems(this.roomtypeData).subscribe(
            data => {
                this.roomtypeList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}