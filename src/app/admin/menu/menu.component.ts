import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../admin.service';


@Component({
    selector: 'Menu',
    templateUrl: 'menu.component.html'
})

export class MenuComponent implements OnInit {
    menuList = [];
    menuData = {};

    constructor(

        private AdminService: AdminService
    ) { }

    ngOnInit() {

        this.AdminService.getMenus().subscribe(
            data => {
                this.menuList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
   
}