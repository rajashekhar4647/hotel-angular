import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'MenuFormComponent',
    templateUrl: 'menuform.component.html'
})

export class MenuFormComponent implements OnInit {
    menuList = [];
    menuidparentList = [];
    menuData = {};
    id: number;
    token;
    moduleList = [];
    moduleNames = [];

    constructor(

        private AdminService: AdminService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
          this.token = sessionStorage.getItem('token');
        
        // this.AdminService.getModules(this.token).subscribe(
        //     data => {
                
        //         this.moduleList = data['result']['data'];
        //         // console.log(this.moduleList);
        //     }, error => {
        //         console.log(error);
        //     });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.AdminService.getMenus().subscribe(
            data => {
                let menuList = data['result']['data'];
                var i;
                for(let i of menuList){
                    if(i.f011fidParent == 0){
                        this.menuidparentList.push(i);
                    }
                 }
                //  console.log(this.menuidparentList);
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        if (this.id > 0) {
            $(".fg-line").addClass('fg-toggled');
            this.AdminService.getMenuDetail(this.id).subscribe(
                data => {
                    this.menuData = data['result'];

                }, error => {
                    console.log(error);
                });
        }
        // console.log(this.id);
    }
    addMenu() {
        console.log(this.menuData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AdminService.updateMenuItems(this.menuData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/menu']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.AdminService.insertMenuItems(this.menuData).subscribe(
                data => {
                    this.router.navigate(['admin/menu']);
                }, error => {
                    console.log(error);
                });

        }


    }
}