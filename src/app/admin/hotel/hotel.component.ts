import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'hotel',
    templateUrl: 'hotel.component.html'
})

export class HotelComponent implements OnInit {
    hotelList =  [];
    hotelData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getHotels().subscribe(
            data => {
                // console.log(data);
                this.hotelList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}