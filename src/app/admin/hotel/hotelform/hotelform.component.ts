import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'HotelFormComponent',
    templateUrl: 'hotelform.component.html'
})

export class HotelFormComponent implements OnInit {
    hotelList =  [];
    hotelData = {};
    cities = [];
    states = [];
    pincodes = [];
    id: number;


    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        if(this.id>0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getHotelDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.hotelData = data['result'];
            }, error => {
                console.log(error);
            });
        }
        
        

        //console.log(this.id);
    }
    addHotel(){
        //    console.log(this.patientData);
           if (this.id > 0) {
            this.adminService.updateHotelItems(this.hotelData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/hotel']);
                }, error => {
                    console.log(error);
                });


        } else {
        this.adminService.insertHotelItems(this.hotelData).subscribe(
            data => {
                this.router.navigate(['admin/hotel']);
        }, error => {
            console.log(error);
        });
    }

    }
}