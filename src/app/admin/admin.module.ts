import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

import {HttpClientModule, HttpClient} from '@angular/common/http';

import { HotelComponent } from "./hotel/hotel.component";
import { HotelFormComponent } from "./hotel/hotelform/hotelform.component";


import { MenuComponent } from "./menu/menu.component";
import { MenuFormComponent } from "./menu/menuform/menuform.component";

import { RoleComponent } from "./role/role.component";
import { RoleFormComponent } from "./role/roleform/roleform.component";

import { RolemenuComponent } from "./rolemenu/rolemenu.component";

import { AdminService } from './admin.service';

import { AdminRoutingModule } from './admin.router.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


import { RoomTypeComponent } from "./roomtype/roomtype.component";
import { RoomTypeFormComponent } from "./roomtype/roomtypeform/roomtypeform.component";

import { RoomComponent } from "./room/room.component";
import { RoomFormComponent } from "./room/roomform/roomform.component";


import { FloorComponent } from "./floor/floor.component";
import { FloorFormComponent } from "./floor/floorform/floorform.component";

import { StaffComponent } from "./staff/staff.component";
import { StaffFormComponent } from "./staff/staffform/staffform.component";

import { BookingComponent } from "./booking/booking.component";
import { BookingFormComponent } from "./booking/bookingform/bookingform.component";

import { PaymentComponent } from "./payment/payment.component";
import { PaymentFormComponent } from "./payment/paymentform/paymentform.component";

import { CleaningComponent } from "./cleaning/cleaning.component";
import { CleaningFormComponent } from "./cleaning/cleaningform/cleaningform.component";

import { InvoiceComponent } from "./invoice/invoice.component";
import { InvoiceFormComponent } from "./invoice/invoiceform/invoiceform.component";

import { OffersComponent } from "./offers/offers.component";
import { EmailTemplateComponent } from "./emailtemplate/emailtemplate.component";
import { EmailTemplateFormComponent } from "./emailtemplate/emailtemplateform/emailtemplateform.component";
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from 'ng2-ckeditor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor} from '../../app/_helpers/jwt.interceptors';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AdminRoutingModule,
        NgSelectModule,
        HttpClientModule,
        Ng2SearchPipeModule,
        CKEditorModule
    ],
    exports: [],
    declarations: [
        RoomTypeComponent,
        RoomTypeFormComponent,
        MenuComponent,
        MenuFormComponent,
        RoleComponent,
        RoleFormComponent,
        RolemenuComponent,
        HotelComponent,
        HotelFormComponent,
        StaffComponent,
        StaffFormComponent,
        FloorComponent,
        FloorFormComponent,
        RoomComponent,
        RoomFormComponent,
        BookingComponent,
        BookingFormComponent,
        PaymentComponent,
        PaymentFormComponent,
        InvoiceComponent,
        InvoiceFormComponent,
        CleaningComponent,
        CleaningFormComponent,
        EmailTemplateComponent,
        EmailTemplateFormComponent,
        OffersComponent

    ],
    providers: [
        
        AdminService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
    ],
})
export class AdminModule { }
