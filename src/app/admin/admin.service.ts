import {
    Injectable
} from '@angular/core';
import {
    HttpHeaders,
    HttpClient
} from '@angular/common/http';
import {
    Observable
} from 'rxjs/Observable';
import {
    environment
} from '../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class AdminService {

    registerUrl: string = environment.api.base + environment.api.endPoints.register;
    accountUrl: string = environment.api.base + environment.api.endPoints.account;

    menuUrl: string = environment.api.base + environment.api.endPoints.menu;
    roleUrl: string = environment.api.base + environment.api.endPoints.role;
    roleMenuUrl: string = environment.api.base + environment.api.endPoints.roleMenu;
    insertRoleMenusUrl: string = environment.api.base + environment.api.endPoints.insertRoleMenus;
    hotelUrl: string = environment.api.base + environment.api.endPoints.hotel;
    staffUrl: string = environment.api.base + environment.api.endPoints.staff;
    roomtypeUrl: string = environment.api.base + environment.api.endPoints.roomtype;
    roomUrl: string = environment.api.base + environment.api.endPoints.room;
    floorUrl: string = environment.api.base + environment.api.endPoints.floor;
    bookingUrl: string = environment.api.base + environment.api.endPoints.booking;
    paymentUrl: string = environment.api.base + environment.api.endPoints.payment;
    invoiceUrl: string = environment.api.base + environment.api.endPoints.invoice;
    cleaningUrl: string = environment.api.base + environment.api.endPoints.cleaning;
    emailtemplateUrl: string = environment.api.base + environment.api.endPoints.emailtemplate;
    sendsmsUrl: string = environment.api.base + environment.api.endPoints.sendsms;
    sendmailUrl: string = environment.api.base + environment.api.endPoints.sendmail;
    

    constructor(private httpClient: HttpClient) {

    }
   
    /*****Room****/
    getRooms() {
        return this.httpClient.get(this.roomUrl, httpOptions);
    }

    getRoomDetail(id) {
        return this.httpClient.get(this.roomUrl + '/' + id, httpOptions);
    }

    insertRoomItems(data): Observable < any > {
        return this.httpClient.post(this.roomUrl, data, httpOptions);
    }


    updateRoomItems(data, id) {

        return this.httpClient.put(this.roomUrl + '/' + id, data, httpOptions);

    }
    /*****Room****/
      /*****Email Template****/
      getEmailTemplates() {
        return this.httpClient.get(this.emailtemplateUrl, httpOptions);
    }

    getEmailTemplateDetail(id) {
        return this.httpClient.get(this.emailtemplateUrl + '/' + id, httpOptions);
    }

    insertEmailTemplateItems(data): Observable < any > {
        return this.httpClient.post(this.emailtemplateUrl, data, httpOptions);
    }


    updateEmailTemplateItems(data, id) {

        return this.httpClient.put(this.emailtemplateUrl + '/' + id, data, httpOptions);

    }
    /*****Email Template****/
    /*****Booking****/
    getBookings() {
        return this.httpClient.get(this.bookingUrl, httpOptions);
    }

    getBookingDetail(id) {
        return this.httpClient.get(this.bookingUrl + '/' + id, httpOptions);
    }

    insertBookingItems(data): Observable < any > {
        return this.httpClient.post(this.bookingUrl, data, httpOptions);
    }


    updateBookingItems(data, id) {

        return this.httpClient.put(this.bookingUrl + '/' + id, data, httpOptions);

    }
    /*****Booking****/
     /*****Floor****/
     getFloors() {
        return this.httpClient.get(this.floorUrl, httpOptions);
    }

    getFloorDetail(id) {
        return this.httpClient.get(this.floorUrl + '/' + id, httpOptions);
    }

    insertFloorItems(data): Observable < any > {
        return this.httpClient.post(this.floorUrl, data, httpOptions);
    }


    updateFloorItems(data, id) {

        return this.httpClient.put(this.floorUrl + '/' + id, data, httpOptions);

    }
    /*****Room****/

    /********RoomType */
    getRoomTypes() {
        return this.httpClient.get(this.roomtypeUrl, httpOptions);
    }

    getRoomTypeDetail(id) {
        return this.httpClient.get(this.roomtypeUrl + '/' + id, httpOptions);
    }

    insertRoomTypeItems(data): Observable < any > {
        return this.httpClient.post(this.roomtypeUrl, data, httpOptions);
    }

    updateRoomTypeItems(data, id) {

        return this.httpClient.put(this.roomtypeUrl + '/' + id, data, httpOptions);

    }
    /********RoomType */
     /********Payment */
     getPayments() {
        return this.httpClient.get(this.paymentUrl, httpOptions);
    }

    getPaymentDetail(id) {
        return this.httpClient.get(this.paymentUrl + '/' + id, httpOptions);
    }

    insertPaymentItems(data): Observable < any > {
        return this.httpClient.post(this.paymentUrl, data, httpOptions);
    }

    updatePaymentItems(data, id) {

        return this.httpClient.put(this.paymentUrl + '/' + id, data, httpOptions);

    }
    /********Payment */
     /********Cleaning */
     getCleanings() {
        return this.httpClient.get(this.cleaningUrl, httpOptions);
    }

    getCleaningDetail(id) {
        return this.httpClient.get(this.cleaningUrl + '/' + id, httpOptions);
    }

    insertCleaningItems(data): Observable < any > {
        return this.httpClient.post(this.cleaningUrl, data, httpOptions);
    }

    updateCleaningItems(data, id) {

        return this.httpClient.put(this.cleaningUrl + '/' + id, data, httpOptions);

    }
    /********Payment */

     /********Invoice */
     getInvoices() {
        return this.httpClient.get(this.invoiceUrl, httpOptions);
    }

    getInvoiceDetail(id) {
        return this.httpClient.get(this.invoiceUrl + '/' + id, httpOptions);
    }

    insertInvoiceItems(data): Observable < any > {
        return this.httpClient.post(this.invoiceUrl, data, httpOptions);
    }

    updateInvoiceItems(data, id) {

        return this.httpClient.put(this.invoiceUrl + '/' + id, data, httpOptions);

    }
    /********Invoice */
    
    /********Menu*/
    getMenus() {
        return this.httpClient.get(this.menuUrl, httpOptions);
    }

    getMenuDetail(id) {
        return this.httpClient.get(this.menuUrl + '/' + id, httpOptions);
    }

    insertMenuItems(data): Observable < any > {
        return this.httpClient.post(this.menuUrl, data, httpOptions);
    }

    updateMenuItems(data, id) {

        return this.httpClient.put(this.menuUrl + '/' + id, data, httpOptions);

    }
    /********Menu*/

    /********Role*/
    getRoles() {
        return this.httpClient.get(this.roleUrl, httpOptions);
    }

    getRoleDetail(id) {
        return this.httpClient.get(this.roleUrl + '/' + id, httpOptions);
    }

    insertRoleItems(data): Observable < any > {
        return this.httpClient.post(this.roleUrl, data, httpOptions);
    }

    updateRoleItems(data, id) {

        return this.httpClient.put(this.roleUrl + '/' + id, data, httpOptions);

    }
    /********Role*/

    /********RoleMenu*/
    getRoleMenus() {
        return this.httpClient.get(this.roleMenuUrl, httpOptions);
    }

    getRoleMenuDetail(id) {
        return this.httpClient.get(this.roleMenuUrl + '/' + id, httpOptions);
    }

    insertRoleMenuItems(data): Observable < any > {
        return this.httpClient.post(this.insertRoleMenusUrl, data, httpOptions);
    }

    /********Role Menu*/
    /********Hotel*/
    getHotels() {
        return this.httpClient.get(this.hotelUrl, httpOptions);
    }

    getHotelDetail(id) {
        return this.httpClient.get(this.hotelUrl + '/' + id, httpOptions);
    }

    insertHotelItems(data): Observable < any > {
        return this.httpClient.post(this.hotelUrl, data, httpOptions);
    }
    updateHotelItems(data, id) {

        return this.httpClient.put(this.hotelUrl + '/' + id, data, httpOptions);

    }
    /********Hotel*/
    /********Staff*/
    getStaffs() {
        return this.httpClient.get(this.staffUrl, httpOptions);
    }

    getStaffDetail(id) {
        return this.httpClient.get(this.staffUrl + '/' + id, httpOptions);
    }

    insertStaffItems(data): Observable < any > {
        return this.httpClient.post(this.staffUrl, data, httpOptions);
    }
    updateStaffItems(data, id) {

        return this.httpClient.put(this.staffUrl + '/' + id, data, httpOptions);

    }
    /********Staff*/


    createUser(data): Observable < any > {
        return this.httpClient.post(this.registerUrl, data, httpOptions);
    }
    updateUser(data, id) {

        return this.httpClient.put(this.accountUrl + '/' + id, data, httpOptions);

    }

    sendSms(data): Observable < any > {
        return this.httpClient.post(this.sendsmsUrl, data, httpOptions);
    }
    sendEmail(data): Observable < any > {
        return this.httpClient.post(this.sendmailUrl, data, httpOptions);
    }
}