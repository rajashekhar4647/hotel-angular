import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AdminService } from '../admin.service';
import * as $ from 'jquery';
@Component({
    selector: 'RolemenuComponent',
    templateUrl: 'rolemenu.component.html'
})

export class RolemenuComponent implements OnInit {
    menuList =  [];
    roleList = [];
    id: number;
    result = [];
    
    constructor(
        private AdminService: AdminService,
        private route: ActivatedRoute,
        private router:Router

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.AdminService.getMenus().subscribe(
            data => {
                this.menuList = data['result']['data'];
                 if(this.id>0) {
                    this.AdminService.getRoleMenuDetail(this.id).subscribe(
                        data => {
                            this.result = data['result'];
        
                            for(var i=0;i<this.menuList.length;i++) {

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.menuList[i]['id']==this.result[j]['idMenu'])
                                    this.menuList[i]['menuStatus'] = true;
                                }
                                
                            }

                            $.each(this.menuList, function(i, val){
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['id'] + "']").prop('checked', true);
                                }

                             });

                    }, error => {
                        console.log(error);
                    });
                   
                }

            }, error => {
                console.log(error);
            });


            this.AdminService.getRoles().subscribe(
                data => {
                    this.roleList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    }
    addRolemenu(){
         var menu = [];
         var allVals = [];
          $('input[name=budget]:checked').each(function() {
            allVals.push($(this).val());
          });


        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        let roleObject = {};
        roleObject['idRole'] = this.id;
        roleObject['idMenu'] = allVals; 
        roleObject['status'] = 1;
           
        this.AdminService.insertRoleMenuItems(roleObject).subscribe(
                    data => {
                   alert("Menu has been updated for Role");
                   this.router.navigate(['admin/role']);

                }, error => {
                    console.log(error);
                });
         
    }
    
}