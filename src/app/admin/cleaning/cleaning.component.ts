import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'cleaning',
    templateUrl: 'cleaning.component.html'
})

export class CleaningComponent implements OnInit {
    cleaningList =  [];
    cleaningData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getCleanings().subscribe(
            data => {
                // console.log(data);
                this.cleaningList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}