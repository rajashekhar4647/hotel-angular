import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'CleaningFormComponent',
    templateUrl: 'cleaningform.component.html'
})

export class CleaningFormComponent implements OnInit {
    cleaningList =  [];
    cleaningData = {};
    rooms = [];
    staffList = []; 
    id: number;


    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.adminService.getRooms().subscribe(
            data => {
                // console.log(data);
                this.rooms = data['result']['data'];
        }, error => {
            console.log(error);
        });
        this.adminService.getStaffs().subscribe(
            data => {
                this.staffList = data['result']['data'];
                // console.log(this.types);
            }, error => {
            console.log(error);
        });
        console.log(this.id);
        if(this.id>0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getCleaningDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.cleaningData = data['result'];
                    this.cleaningData['idStaff'] = parseInt(this.cleaningData['idStaff']);
                    this.cleaningData['idRoom'] = parseInt(this.cleaningData['idRoom']);
            }, error => {
                console.log(error);
            });
        }
        
        

        //console.log(this.id);
    }
    addCleaning(){
        //    console.log(this.patientData);
           if (this.id > 0) {
            this.adminService.updateCleaningItems(this.cleaningData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/cleaning']);
                }, error => {
                    console.log(error);
                });


        } else {
        this.adminService.insertCleaningItems(this.cleaningData).subscribe(
            data => {
                this.router.navigate(['admin/cleaning']);
        }, error => {
            console.log(error);
        });
    }

    }
}