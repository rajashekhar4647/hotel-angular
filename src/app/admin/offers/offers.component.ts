import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../admin.service'


@Component({
    selector: 'offers',
    templateUrl: 'offers.component.html'
})

export class OffersComponent implements OnInit {
    templateList = [];
    offerData = {};

    constructor(
        
        private AdminService: AdminService
    ) { }

    ngOnInit() { 
        
        this.AdminService.getEmailTemplates().subscribe(
            data => {
                this.templateList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    sendmail(){
        if(this.offerData['email'] == '' || this.offerData['email'] == undefined){
            alert("Please select template");
            return false;
        }
        this.AdminService.sendEmail(this.offerData).subscribe(
            data => {
                alert('Emails sent successfully');
        }, error => {
            console.log(error);
        });
    }
    sendsms(){
        if(this.offerData['sms'] == '' || this.offerData['sms'] == undefined){
            alert("Please enter message");
            return false;
        }
        this.AdminService.sendSms(this.offerData).subscribe(
            data => {
                alert('Messages sent successfully');
        }, error => {
            console.log(error);
        });
    }
}