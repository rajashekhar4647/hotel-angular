import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

@Injectable()
export class AuthTokenInterceptorService implements HttpInterceptor {
    
    intercept(request: HttpRequest<any>, next: HttpHandler) {
    
    request = request.clone({
        setHeaders: {
            Authorization: `Beareradsfdf`
        }
    });
        
    return next.handle(request);
  }
}