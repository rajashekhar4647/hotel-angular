import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'invoice',
    templateUrl: 'invoice.component.html'
})

export class InvoiceComponent implements OnInit {
    invoiceList =  [];
    invoiceData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getInvoices().subscribe(
            data => {
                // console.log(data);
                this.invoiceList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}