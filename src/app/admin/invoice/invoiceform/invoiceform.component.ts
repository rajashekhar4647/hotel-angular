import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'InvoiceFormComponent',
    templateUrl: 'invoiceform.component.html'
}) 
 
export class InvoiceFormComponent implements OnInit {
    paymentList =  [];
    invoiceData = {};
    rooms = []; 
    id: number;


    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.adminService.getPayments().subscribe(
            data => {
                // console.log(data);
                this.paymentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
        console.log(this.id);
        if(this.id>0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getInvoiceDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.invoiceData = data['result'];
                    // this.invoiceData['idPayment'] = parseInt(this.invoiceData['idPayment ']);
            }, error => {
                console.log(error);
            });
        }
    }
    addInvoice(){
        //    console.log(this.patientData);
           if (this.id > 0) {
            this.adminService.updateInvoiceItems(this.invoiceData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/invoice']);
                }, error => {
                    console.log(error);
                });


        } else {
        this.adminService.insertInvoiceItems(this.invoiceData).subscribe(
            data => {
                this.router.navigate(['admin/invoice']);
        }, error => {
            console.log(error);
        });
    }

    }
}