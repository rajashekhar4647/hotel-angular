import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'RoomFormComponent',
    templateUrl: 'roomform.component.html'
})

export class RoomFormComponent implements OnInit {
    roomList =  [];
    roomData = {};
    floors = [];
    types = []; 
    pincodes = [];
    id: number;
    usages = [];


    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,

    ) { }

    ngOnInit() { 
        let usageObj = {};
        usageObj['name'] = "Booking";
        usageObj['value'] = 1;
        this.usages.push(usageObj);
        usageObj = {};
        usageObj['name'] = "Blocked";
        usageObj['value'] = 2;
        this.usages.push(usageObj);
        usageObj = {};
        usageObj['name'] = "Only Staff";
        usageObj['value'] = 2;
        this.usages.push(usageObj);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.adminService.getFloors().subscribe(
            data => {
                // console.log(data);
                this.floors = data['result']['data'];
        }, error => {
            console.log(error);
        });
        this.adminService.getRoomTypes().subscribe(
            data => {
                this.types = data['result']['data'];
                console.log(this.types);
            }, error => {
            console.log(error);
        });
        console.log(this.id);
        if(this.id>0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getRoomDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.roomData = data['result'];
                    this.roomData['idType'] = parseInt(this.roomData['idType']);
                    this.roomData['idFloor'] = parseInt(this.roomData['idFloor']);
                    this.roomData['usage'] = parseInt(this.roomData['usage']);
            }, error => {
                console.log(error);
            });
        }
        
        

        //console.log(this.id);
    }
    addRoom(){
        //    console.log(this.patientData);
           if (this.id > 0) {
            this.adminService.updateRoomItems(this.roomData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/room']);
                }, error => {
                    console.log(error);
                });


        } else {
        this.adminService.insertRoomItems(this.roomData).subscribe(
            data => {
                this.router.navigate(['admin/room']);
        }, error => {
            console.log(error);
        });
    }

    }
}