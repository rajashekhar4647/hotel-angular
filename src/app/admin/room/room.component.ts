import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'room',
    templateUrl: 'room.component.html'
})

export class RoomComponent implements OnInit {
    roomList =  [];
    roomData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getRooms().subscribe(
            data => {
                // console.log(data);
                this.roomList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}