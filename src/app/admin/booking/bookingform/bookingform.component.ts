import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'BookingFormComponent',
    templateUrl: 'bookingform.component.html'
})

export class BookingFormComponent implements OnInit {
    bookingList = [];
    bookingData = {};
    rooms = [];
    id: number;


    constructor(

        private adminService: AdminService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.adminService.getRooms().subscribe(
            data => {
                // console.log(data);
                this.rooms = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);
        if (this.id > 0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getBookingDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.bookingData = data['result'];
                    this.bookingData['idRoom'] = parseInt(this.bookingData['idRoom']);
                }, error => {
                    console.log(error);
                });
        }



        //console.log(this.id);
    }
    getAmount(){
        this.adminService.getRoomDetail(this.bookingData['idRoom']).subscribe(
            data => {
                this.bookingData['amount'] = data['result']['originalPrice'];
            }, error => {
                console.log(error);
            });
    }
    addBooking() {
        //    console.log(this.patientData);
        if (this.id > 0) {
            this.adminService.updateBookingItems(this.bookingData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/booking']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.adminService.insertBookingItems(this.bookingData).subscribe(
                data => {
                    this.router.navigate(['admin/booking']);
                }, error => {
                    console.log(error);
                });
        }

    }
}