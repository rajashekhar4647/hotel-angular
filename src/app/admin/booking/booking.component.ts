import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'booking',
    templateUrl: 'booking.component.html'
})

export class BookingComponent implements OnInit {
    bookingList =  [];
    bookingData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getBookings().subscribe(
            data => {
                // console.log(data);
                this.bookingList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}