import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'PaymentFormComponent',
    templateUrl: 'paymentform.component.html'
})
 
export class PaymentFormComponent implements OnInit {
    paymentList =  [];
    types =  [];
    paymentData = {};
    invoices = []; 
    id: number;
    balanceAmount;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,

    ) { }

    ngOnInit() { 
        let typeobj = {};
        typeobj['name'] = 'Cash';
        this.types.push(typeobj);
        typeobj = {};
        typeobj['name'] = 'Cheque';
        this.types.push(typeobj);
        typeobj = {};
        typeobj['name'] = 'Bank';
        this.types.push(typeobj);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.adminService.getInvoices().subscribe(
            data => {
                // console.log(data);
                this.invoices = data['result']['data'];
        }, error => {
            console.log(error);
        });
        console.log(this.id);
        if(this.id>0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getPaymentDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.paymentData = data['result'];
                    this.paymentData['idRoom'] = parseInt(this.paymentData['idRoom']);
            }, error => {
                console.log(error);
            });
        }
        
        

        //console.log(this.id);
    }
    addPayment(){
        //    console.log(this.patientData);
           if (this.id > 0) {
            this.adminService.updatePaymentItems(this.paymentData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/payment']);
                }, error => {
                    console.log(error);
                });


        } else {
        this.adminService.insertPaymentItems(this.paymentData).subscribe(
            data => {
                this.router.navigate(['admin/payment']);
        }, error => {
            console.log(error);
        });
    }

    }
    getInvoiceDetails(){
            this.adminService.getInvoiceDetail(this.paymentData['idInvoice']).subscribe(
                data => {
            $(".fg-line").addClass('fg-toggled');
            this.balanceAmount = data['result']['balance'];;
                    this.paymentData['amount'] = data['result']['balance'];
                }, error => {
                    console.log(error);
                });
    }
    checkBalance(){
        if(parseFloat(this.balanceAmount) <  parseFloat(this.paymentData['amount'])){
            console.log(this.balanceAmount+'-'+this.paymentData['amount']);
            alert("Amount cannot be greater than balance");
            this.paymentData['amount'] = this.balanceAmount;
            return false;
        }
    }
}