import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'payment',
    templateUrl: 'payment.component.html'
})

export class PaymentComponent implements OnInit {
    paymentList =  [];
    paymentData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getPayments().subscribe(
            data => {
                // console.log(data);
                this.paymentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}