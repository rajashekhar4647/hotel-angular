import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../admin.service';

@Component({
    selector: 'EmailTemplateFormComponent',
    templateUrl: 'emailtemplateform.component.html'
})

export class EmailTemplateFormComponent implements OnInit {
    emailtemplateList =  [];
    emailtemplateData = {};
    id: number;


    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        
        if(this.id>0) {
            $(".fg-line").addClass('fg-toggled');

            this.adminService.getEmailTemplateDetail(this.id).subscribe(
                data => {
                    // console.log(data);
                    this.emailtemplateData = data['result'];

    
            }, error => {
                console.log(error);
            });
        }
        

        //console.log(this.id);
    }
    addEmailTemplate(){
        //    console.log(this.patientData);
           if (this.id > 0) {

            this.adminService.updateEmailTemplateItems(this.emailtemplateData, this.id).subscribe(
                data => {
                    this.router.navigate(['admin/emailtemplate']);
                }, error => {
                    console.log(error);
                });


        } else {
        this.adminService.insertEmailTemplateItems(this.emailtemplateData).subscribe(
            data => {
                this.router.navigate(['admin/emailtemplate']);
        }, error => {
            console.log(error);
        });
    }

    }
}