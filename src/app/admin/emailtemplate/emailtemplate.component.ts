import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
@Component({
    selector: 'emailtemplate',
    templateUrl: 'emailtemplate.component.html'
})

export class EmailTemplateComponent implements OnInit {
    emailtemplateList =  [];
    emailtemplateData = {};
    id: any;

    constructor(
        
        private adminService: AdminService,
        private route: ActivatedRoute,
        private router:Router,
        location : Location
    ) { }

    ngOnInit() { 
        
        this.adminService.getEmailTemplates().subscribe(
            data => {
                // console.log(data);
                this.emailtemplateList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}