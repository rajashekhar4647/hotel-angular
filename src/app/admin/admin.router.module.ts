import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MenuComponent } from "./menu/menu.component";
import { MenuFormComponent } from "./menu/menuform/menuform.component";

import { RoleComponent } from "./role/role.component";
import { RoleFormComponent } from "./role/roleform/roleform.component";

import { RolemenuComponent } from "./rolemenu/rolemenu.component";


import { StaffComponent } from "./staff/staff.component";
import { StaffFormComponent } from "./staff/staffform/staffform.component";

import { HotelComponent } from "./hotel/hotel.component";
import { HotelFormComponent } from "./hotel/hotelform/hotelform.component";

import { RoomTypeComponent } from "./roomtype/roomtype.component";
import { RoomTypeFormComponent } from "./roomtype/roomtypeform/roomtypeform.component";

import { FloorComponent } from "./floor/floor.component";
import { FloorFormComponent } from "./floor/floorform/floorform.component";
import { RoomComponent } from "./room/room.component";
import { RoomFormComponent } from "./room/roomform/roomform.component";

import { BookingComponent } from "./booking/booking.component";
import { BookingFormComponent } from "./booking/bookingform/bookingform.component";

import { PaymentComponent } from "./payment/payment.component";
import { PaymentFormComponent } from "./payment/paymentform/paymentform.component";

import { CleaningComponent } from "./cleaning/cleaning.component";
import { CleaningFormComponent } from "./cleaning/cleaningform/cleaningform.component";

import { InvoiceComponent } from "./invoice/invoice.component";
import { InvoiceFormComponent } from "./invoice/invoiceform/invoiceform.component";

import { EmailTemplateComponent } from "./emailtemplate/emailtemplate.component";
import { EmailTemplateFormComponent } from "./emailtemplate/emailtemplateform/emailtemplateform.component";
import { OffersComponent } from "./offers/offers.component";

const routes: Routes = [

  { path: 'roomtype', component: RoomTypeComponent },
  { path: 'addroomtype', component: RoomTypeFormComponent },
  { path: 'editroomtype/:id', component: RoomTypeFormComponent },

  { path: 'menu', component: MenuComponent },
  { path: 'addmenu', component: MenuFormComponent },
  { path: 'editmenu/:id', component: MenuFormComponent },

  { path: 'floor', component: FloorComponent },
  { path: 'addfloor', component: FloorFormComponent },
  { path: 'editfloor/:id', component: FloorFormComponent },

  { path: 'role', component: RoleComponent },
  { path: 'addrole', component: RoleFormComponent },
  { path: 'editrole/:id', component: RoleFormComponent },

  { path: 'rolemenu/:id', component: RolemenuComponent },

  { path: 'hotel', component: HotelComponent },
  { path: 'addhotel', component: HotelFormComponent },
  { path: 'edithotel/:id', component: HotelFormComponent },

  { path: 'staff', component: StaffComponent },
  { path: 'addstaff', component: StaffFormComponent },
  { path: 'editstaff/:id', component: StaffFormComponent },
  

  { path: 'room', component: RoomComponent },
  { path: 'addroom', component: RoomFormComponent },
  { path: 'editroom/:id', component: RoomFormComponent },

  { path: 'booking', component: BookingComponent },
  { path: 'addbooking', component: BookingFormComponent },
  { path: 'editbooking/:id', component: BookingFormComponent },
  
  { path: 'payment', component: PaymentComponent },
  { path: 'addpayment', component: PaymentFormComponent },
  { path: 'editpayment/:id', component: PaymentFormComponent },

  { path: 'cleaning', component: CleaningComponent },
  { path: 'addcleaning', component: CleaningFormComponent },
  { path: 'editcleaning/:id', component: CleaningFormComponent },
  
  { path: 'invoice', component: InvoiceComponent },
  { path: 'addinvoice', component: InvoiceFormComponent },
  { path: 'editinvoice/:id', component: InvoiceFormComponent },

  { path: 'emailtemplate', component: EmailTemplateComponent },
  { path: 'addemailtemplate', component: EmailTemplateFormComponent },
  { path: 'editemailtemplate/:id', component: EmailTemplateFormComponent },

  { path: 'offers', component: OffersComponent },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }