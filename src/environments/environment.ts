// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  api: {
    // base: 'http://localhost:8080/v1/',
    base: 'http://18.191.136.113:83/v1/',
    endPoints: {
      login : 'login',
      register: 'register',
      account : 'account',
      menu : 'menu',
      role : 'role',
      roleMenu : 'roleMenus',
      menus:'menuRole',
      insertRoleMenus : 'insertRoleMenus',
      hotel:'hotel',
      staff:'staff',
      roomtype:'roomType',
      room:'room',
      floor:'floor',
      booking:'booking',
      payment:'payment',
      invoice:'invoice',
      cleaning:'cleaning',
      emailtemplate:'emailTemplate',
      sendmail:'sendEmail',
      sendsms:'sendSms'
    }
  }
};
