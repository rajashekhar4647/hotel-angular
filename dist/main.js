(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"app/admin/admin.module": [
		"./src/app/admin/admin.module.ts",
		"app-admin-admin-module"
	],
	"app/auth/auth.module": [
		"./src/app/auth/auth.module.ts",
		"app-auth-auth-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/_guards/auth.guard.service.ts":
/*!***********************************************!*\
  !*** ./src/app/_guards/auth.guard.service.ts ***!
  \***********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (sessionStorage.getItem('hospitalUserToken')) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login']);
        //        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/_helpers/jwt.interceptors.ts":
/*!**********************************************!*\
  !*** ./src/app/_helpers/jwt.interceptors.ts ***!
  \**********************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor(spinner) {
        this.spinner = spinner;
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (event) {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                _this.spinner.hide();
            }
            else {
                _this.spinner.show();
            }
            return event;
        }));
    };
    JwtInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"]])
    ], JwtInterceptor);
    return JwtInterceptor;
}());



/***/ }),

/***/ "./src/app/admin/admin.service.ts":
/*!****************************************!*\
  !*** ./src/app/admin/admin.service.ts ***!
  \****************************************/
/*! exports provided: AdminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminService", function() { return AdminService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};
var AdminService = /** @class */ (function () {
    function AdminService(httpClient) {
        this.httpClient = httpClient;
        this.registerUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.register;
        this.accountUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.account;
        this.menuUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.menu;
        this.roleUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.role;
        this.roleMenuUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.roleMenu;
        this.insertRoleMenusUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.insertRoleMenus;
        this.hotelUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.hotel;
        this.staffUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.staff;
        this.roomtypeUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.roomtype;
        this.roomUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.room;
        this.floorUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.floor;
        this.bookingUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.booking;
        this.paymentUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.payment;
        this.invoiceUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.invoice;
        this.cleaningUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.cleaning;
        this.emailtemplateUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.emailtemplate;
        this.sendsmsUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.sendsms;
        this.sendmailUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.sendmail;
    }
    /*****Room****/
    AdminService.prototype.getRooms = function () {
        return this.httpClient.get(this.roomUrl, httpOptions);
    };
    AdminService.prototype.getRoomDetail = function (id) {
        return this.httpClient.get(this.roomUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertRoomItems = function (data) {
        return this.httpClient.post(this.roomUrl, data, httpOptions);
    };
    AdminService.prototype.updateRoomItems = function (data, id) {
        return this.httpClient.put(this.roomUrl + '/' + id, data, httpOptions);
    };
    /*****Room****/
    /*****Email Template****/
    AdminService.prototype.getEmailTemplates = function () {
        return this.httpClient.get(this.emailtemplateUrl, httpOptions);
    };
    AdminService.prototype.getEmailTemplateDetail = function (id) {
        return this.httpClient.get(this.emailtemplateUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertEmailTemplateItems = function (data) {
        return this.httpClient.post(this.emailtemplateUrl, data, httpOptions);
    };
    AdminService.prototype.updateEmailTemplateItems = function (data, id) {
        return this.httpClient.put(this.emailtemplateUrl + '/' + id, data, httpOptions);
    };
    /*****Email Template****/
    /*****Booking****/
    AdminService.prototype.getBookings = function () {
        return this.httpClient.get(this.bookingUrl, httpOptions);
    };
    AdminService.prototype.getBookingDetail = function (id) {
        return this.httpClient.get(this.bookingUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertBookingItems = function (data) {
        return this.httpClient.post(this.bookingUrl, data, httpOptions);
    };
    AdminService.prototype.updateBookingItems = function (data, id) {
        return this.httpClient.put(this.bookingUrl + '/' + id, data, httpOptions);
    };
    /*****Booking****/
    /*****Floor****/
    AdminService.prototype.getFloors = function () {
        return this.httpClient.get(this.floorUrl, httpOptions);
    };
    AdminService.prototype.getFloorDetail = function (id) {
        return this.httpClient.get(this.floorUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertFloorItems = function (data) {
        return this.httpClient.post(this.floorUrl, data, httpOptions);
    };
    AdminService.prototype.updateFloorItems = function (data, id) {
        return this.httpClient.put(this.floorUrl + '/' + id, data, httpOptions);
    };
    /*****Room****/
    /********RoomType */
    AdminService.prototype.getRoomTypes = function () {
        return this.httpClient.get(this.roomtypeUrl, httpOptions);
    };
    AdminService.prototype.getRoomTypeDetail = function (id) {
        return this.httpClient.get(this.roomtypeUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertRoomTypeItems = function (data) {
        return this.httpClient.post(this.roomtypeUrl, data, httpOptions);
    };
    AdminService.prototype.updateRoomTypeItems = function (data, id) {
        return this.httpClient.put(this.roomtypeUrl + '/' + id, data, httpOptions);
    };
    /********RoomType */
    /********Payment */
    AdminService.prototype.getPayments = function () {
        return this.httpClient.get(this.paymentUrl, httpOptions);
    };
    AdminService.prototype.getPaymentDetail = function (id) {
        return this.httpClient.get(this.paymentUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertPaymentItems = function (data) {
        return this.httpClient.post(this.paymentUrl, data, httpOptions);
    };
    AdminService.prototype.updatePaymentItems = function (data, id) {
        return this.httpClient.put(this.paymentUrl + '/' + id, data, httpOptions);
    };
    /********Payment */
    /********Cleaning */
    AdminService.prototype.getCleanings = function () {
        return this.httpClient.get(this.cleaningUrl, httpOptions);
    };
    AdminService.prototype.getCleaningDetail = function (id) {
        return this.httpClient.get(this.cleaningUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertCleaningItems = function (data) {
        return this.httpClient.post(this.cleaningUrl, data, httpOptions);
    };
    AdminService.prototype.updateCleaningItems = function (data, id) {
        return this.httpClient.put(this.cleaningUrl + '/' + id, data, httpOptions);
    };
    /********Payment */
    /********Invoice */
    AdminService.prototype.getInvoices = function () {
        return this.httpClient.get(this.invoiceUrl, httpOptions);
    };
    AdminService.prototype.getInvoiceDetail = function (id) {
        return this.httpClient.get(this.invoiceUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertInvoiceItems = function (data) {
        return this.httpClient.post(this.invoiceUrl, data, httpOptions);
    };
    AdminService.prototype.updateInvoiceItems = function (data, id) {
        return this.httpClient.put(this.invoiceUrl + '/' + id, data, httpOptions);
    };
    /********Invoice */
    /********Menu*/
    AdminService.prototype.getMenus = function () {
        return this.httpClient.get(this.menuUrl, httpOptions);
    };
    AdminService.prototype.getMenuDetail = function (id) {
        return this.httpClient.get(this.menuUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertMenuItems = function (data) {
        return this.httpClient.post(this.menuUrl, data, httpOptions);
    };
    AdminService.prototype.updateMenuItems = function (data, id) {
        return this.httpClient.put(this.menuUrl + '/' + id, data, httpOptions);
    };
    /********Menu*/
    /********Role*/
    AdminService.prototype.getRoles = function () {
        return this.httpClient.get(this.roleUrl, httpOptions);
    };
    AdminService.prototype.getRoleDetail = function (id) {
        return this.httpClient.get(this.roleUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertRoleItems = function (data) {
        return this.httpClient.post(this.roleUrl, data, httpOptions);
    };
    AdminService.prototype.updateRoleItems = function (data, id) {
        return this.httpClient.put(this.roleUrl + '/' + id, data, httpOptions);
    };
    /********Role*/
    /********RoleMenu*/
    AdminService.prototype.getRoleMenus = function () {
        return this.httpClient.get(this.roleMenuUrl, httpOptions);
    };
    AdminService.prototype.getRoleMenuDetail = function (id) {
        return this.httpClient.get(this.roleMenuUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertRoleMenuItems = function (data) {
        return this.httpClient.post(this.insertRoleMenusUrl, data, httpOptions);
    };
    /********Role Menu*/
    /********Hotel*/
    AdminService.prototype.getHotels = function () {
        return this.httpClient.get(this.hotelUrl, httpOptions);
    };
    AdminService.prototype.getHotelDetail = function (id) {
        return this.httpClient.get(this.hotelUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertHotelItems = function (data) {
        return this.httpClient.post(this.hotelUrl, data, httpOptions);
    };
    AdminService.prototype.updateHotelItems = function (data, id) {
        return this.httpClient.put(this.hotelUrl + '/' + id, data, httpOptions);
    };
    /********Hotel*/
    /********Staff*/
    AdminService.prototype.getStaffs = function () {
        return this.httpClient.get(this.staffUrl, httpOptions);
    };
    AdminService.prototype.getStaffDetail = function (id) {
        return this.httpClient.get(this.staffUrl + '/' + id, httpOptions);
    };
    AdminService.prototype.insertStaffItems = function (data) {
        return this.httpClient.post(this.staffUrl, data, httpOptions);
    };
    AdminService.prototype.updateStaffItems = function (data, id) {
        return this.httpClient.put(this.staffUrl + '/' + id, data, httpOptions);
    };
    /********Staff*/
    AdminService.prototype.createUser = function (data) {
        return this.httpClient.post(this.registerUrl, data, httpOptions);
    };
    AdminService.prototype.updateUser = function (data, id) {
        return this.httpClient.put(this.accountUrl + '/' + id, data, httpOptions);
    };
    AdminService.prototype.sendSms = function (data) {
        return this.httpClient.post(this.sendsmsUrl, data, httpOptions);
    };
    AdminService.prototype.sendEmail = function (data) {
        return this.httpClient.post(this.sendmailUrl, data, httpOptions);
    };
    AdminService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AdminService);
    return AdminService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ps-right-navbar{\n    float:right;\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n     \n    <router-outlet></router-outlet>\n    <ngx-spinner\n    bdColor = \"rgba(223,220,220,0.52)\"\n    size = \"default\"\n    color = \"#2010ea\"\n    type = \"square-jelly-box\"\n    ></ngx-spinner>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")],
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material/material.module */ "./src/app/material/material.module.ts");
/* harmony import */ var _app_router_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.router.module */ "./src/app/app.router.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _layout_header_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./layout/header/header.component */ "./src/app/layout/header/header.component.ts");
/* harmony import */ var _layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./layout/footer/footer.component */ "./src/app/layout/footer/footer.component.ts");
/* harmony import */ var _layout_doctor_doctorlayout_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./layout/doctor/doctorlayout.component */ "./src/app/layout/doctor/doctorlayout.component.ts");
/* harmony import */ var _layout_admin_adminlayout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./layout/admin/adminlayout.component */ "./src/app/layout/admin/adminlayout.component.ts");
/* harmony import */ var _layout_default_defaultlayout_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./layout/default/defaultlayout.component */ "./src/app/layout/default/defaultlayout.component.ts");
/* harmony import */ var _errors_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./errors/page-not-found/page-not-found.component */ "./src/app/errors/page-not-found/page-not-found.component.ts");
/* harmony import */ var _app_helpers_jwt_interceptors__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./../app/_helpers/jwt.interceptors */ "./src/app/_helpers/jwt.interceptors.ts");
/* harmony import */ var _admin_admin_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./admin/admin.service */ "./src/app/admin/admin.service.ts");
/* harmony import */ var _layout_layout_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./layout/layout.service */ "./src/app/layout/layout.service.ts");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var _guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./_guards/auth.guard.service */ "./src/app/_guards/auth.guard.service.ts");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var ng2_ckeditor__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ng2-ckeditor */ "./node_modules/ng2-ckeditor/lib/ng2-ckeditor.js");
/* harmony import */ var ng2_ckeditor__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(ng2_ckeditor__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _layout_header_header_component__WEBPACK_IMPORTED_MODULE_9__["HeaderComponent"],
                _layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
                _errors_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_14__["PageNotFoundComponent"],
                _layout_doctor_doctorlayout_component__WEBPACK_IMPORTED_MODULE_11__["DoctorlayoutComponent"],
                _layout_admin_adminlayout_component__WEBPACK_IMPORTED_MODULE_12__["AdminlayoutComponent"],
                _layout_default_defaultlayout_component__WEBPACK_IMPORTED_MODULE_13__["DefaultlayoutComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["NoopAnimationsModule"],
                _material_material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_18__["Ng2SearchPipeModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
                ng2_ckeditor__WEBPACK_IMPORTED_MODULE_21__["CKEditorModule"],
                ngx_spinner__WEBPACK_IMPORTED_MODULE_22__["NgxSpinnerModule"],
                _app_router_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"] // Should be last import
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["NO_ERRORS_SCHEMA"]],
            providers: [
                _admin_admin_service__WEBPACK_IMPORTED_MODULE_16__["AdminService"],
                _layout_layout_service__WEBPACK_IMPORTED_MODULE_17__["LayoutService"],
                _guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_19__["AuthGuard"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                    useClass: _app_helpers_jwt_interceptors__WEBPACK_IMPORTED_MODULE_15__["JwtInterceptor"],
                    multi: true
                }
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.router.module.ts":
/*!**************************************!*\
  !*** ./src/app/app.router.module.ts ***!
  \**************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _errors_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./errors/page-not-found/page-not-found.component */ "./src/app/errors/page-not-found/page-not-found.component.ts");
/* harmony import */ var _layout_admin_adminlayout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout/admin/adminlayout.component */ "./src/app/layout/admin/adminlayout.component.ts");
/* harmony import */ var _layout_default_defaultlayout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layout/default/defaultlayout.component */ "./src/app/layout/default/defaultlayout.component.ts");
/* harmony import */ var _guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_guards/auth.guard.service */ "./src/app/_guards/auth.guard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: 'auth', component: _layout_default_defaultlayout_component__WEBPACK_IMPORTED_MODULE_4__["DefaultlayoutComponent"], loadChildren: 'app/auth/auth.module#AuthModule', canActivate: [_guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: 'admin', component: _layout_admin_adminlayout_component__WEBPACK_IMPORTED_MODULE_3__["AdminlayoutComponent"], loadChildren: 'app/admin/admin.module#AdminModule', canActivate: [_guards_auth_guard_service__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    { path: '', component: _layout_default_defaultlayout_component__WEBPACK_IMPORTED_MODULE_4__["DefaultlayoutComponent"], loadChildren: 'app/auth/auth.module#AuthModule' },
    { path: '**', component: _errors_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_2__["PageNotFoundComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/errors/page-not-found/page-not-found.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/errors/page-not-found/page-not-found.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n    <div class=\"row\">\n        <div class=\"col-md-8 col-md-offset-2 text-center\">\n            <div class=\"form-container\">\n                <mat-card>\n                    <h1>404</h1>\n                    <h3>SORRY</h3>\n                    <p>The page you're looking for was not found.</p>\n                    <button (click)=\"goBack()\" mat-raised-button color=\"primary\">GO BACK</button>\n                </mat-card>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/errors/page-not-found/page-not-found.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/errors/page-not-found/page-not-found.component.ts ***!
  \*******************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent(location) {
        this.location = location;
    }
    PageNotFoundComponent.prototype.goBack = function () {
        this.location.back();
    };
    PageNotFoundComponent.prototype.ngOnInit = function () { };
    PageNotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/errors/page-not-found/page-not-found.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/layout/admin/adminlayout.component.html":
/*!*********************************************************!*\
  !*** ./src/app/layout/admin/adminlayout.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"container-fluid main-wrapper\">\n        <div class=\"row\">\n          <aside class=\"col-sm-4 col-lg-3 sidebar\">        \n              <ul class=\"sidebar-menu clearfix\">\n                    <li *ngFor=\"let menu of menuList\" class=\"{{menu.active}}\">\n                            <a  routerLink='{{menu.link}}' class=\"{{menu.class}}\" (click)=\"makeActive(menu.menuName)\">{{menu.menuName}}</a>\n                          </li>  \n              </ul>      \n          </aside>\n          \n<router-outlet></router-outlet>\n</div>\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/layout/admin/adminlayout.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/layout/admin/adminlayout.component.ts ***!
  \*******************************************************/
/*! exports provided: AdminlayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminlayoutComponent", function() { return AdminlayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _layout_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../layout.service */ "./src/app/layout/layout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AdminlayoutComponent = /** @class */ (function () {
    function AdminlayoutComponent(layoutService) {
        this.layoutService = layoutService;
        this.menuList = [];
        this.module = 'admin';
    }
    AdminlayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.token = sessionStorage.getItem('hospitalUserToken');
        this.link = this.module + '/' + this.token;
        this.layoutService.getMenus(this.link).subscribe(function (data) {
            _this.menuList = data['result']['data'];
            // console.log(this.menuList);
        }, function (error) {
            console.log(error);
        });
    };
    AdminlayoutComponent.prototype.makeActive = function (id) {
        for (var i = 0; i < this.menuList.length; i++) {
            this.menuList[i]['active'] = '';
        }
        for (var i = 0; i < this.menuList.length; i++) {
            if (id == this.menuList[i]['menuName']) {
                //   console.log(this.menuList['menuName']);
                this.menuList[i]['active'] = 'active';
                return;
            }
            else {
                this.menuList[i]['active'] = '';
            }
        }
        console.log(this.menuList);
        //   console.log(id);
    };
    AdminlayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'admin-layout',
            template: __webpack_require__(/*! ./adminlayout.component.html */ "./src/app/layout/admin/adminlayout.component.html")
        }),
        __metadata("design:paramtypes", [_layout_service__WEBPACK_IMPORTED_MODULE_1__["LayoutService"]])
    ], AdminlayoutComponent);
    return AdminlayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/default/defaultlayout.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/default/defaultlayout.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/layout/default/defaultlayout.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/default/defaultlayout.component.ts ***!
  \***********************************************************/
/*! exports provided: DefaultlayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultlayoutComponent", function() { return DefaultlayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DefaultlayoutComponent = /** @class */ (function () {
    function DefaultlayoutComponent() {
    }
    DefaultlayoutComponent.prototype.ngOnInit = function () {
    };
    DefaultlayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'default-layout',
            template: __webpack_require__(/*! ./defaultlayout.component.html */ "./src/app/layout/default/defaultlayout.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], DefaultlayoutComponent);
    return DefaultlayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/doctor/doctorlayout.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layout/doctor/doctorlayout.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<section class=\"ps-main-section\">\n    <div class=\"ps-left-navbar\">\n        <ul class=\"ps-left-navbar-menu\" style=\"color: white;\">\n\n            <li class=\"ps-left-navbar-item\">\n                <a [routerLink]=\"['/doctor/patient']\">Patients </a>\n            </li>\n            <li class=\"ps-left-navbar-item\">\n                    <a [routerLink]=\"['/doctor/lab']\">Lab </a>\n                </li>\n                <li class=\"ps-left-navbar-item\">\n                    <a [routerLink]=\"['/doctor/summary']\">Summary </a>\n                </li>\n</ul>\n</div>\n</section>\n<div *ngIf=\"patientId; then content;\"></div>\n    <ng-template #content>\n    <div class=\"ps-right-navbar\" >\n            \n            <patient-details ></patient-details>\n        </div>\n    </ng-template> \n<router-outlet></router-outlet>\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/layout/doctor/doctorlayout.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/doctor/doctorlayout.component.ts ***!
  \*********************************************************/
/*! exports provided: DoctorlayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorlayoutComponent", function() { return DoctorlayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DoctorlayoutComponent = /** @class */ (function () {
    function DoctorlayoutComponent() {
    }
    DoctorlayoutComponent.prototype.ngOnInit = function () {
        this.patientId = sessionStorage.getItem('patientId');
        if (this.patientId) {
            this.patientId = true;
        }
        else {
            this.patientId = false;
        }
    };
    DoctorlayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'doctor-layout',
            template: __webpack_require__(/*! ./doctorlayout.component.html */ "./src/app/layout/doctor/doctorlayout.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], DoctorlayoutComponent);
    return DoctorlayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/footer/footer.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"text-center\">\n        <p>\n            ©2019 Copyright:\n             <a routerLink=\"/\">Hotel Management System</a>\n        </p>\n        \n    </div>\n</div>"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () { };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/layout/footer/footer.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/header/header.component.css":
/*!****************************************************!*\
  !*** ./src/app/layout/header/header.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fill-row{\n    flex: 1 1 auto;\n}\n\n.navbar-brand > a {\n  color: #FFFFFF !important;\n}"

/***/ }),

/***/ "./src/app/layout/header/header.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n        <nav class=\"navbar navbar-default dashboard-navbar\">\n                <div class=\"container-fluid\">\n                  <div class=\"navbar-header\">\n                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n                      <span class=\"sr-only\">Toggle navigation</span>\n                      <span class=\"icon-bar\"></span>\n                      <span class=\"icon-bar\"></span>\n                      <span class=\"icon-bar\"></span>\n                    </button>\n                    <!-- <a class=\"navbar-brand\" href=\"#\">Health Politans</a> -->\n                  </div>\n                  <div id=\"navbar\" class=\"navbar-collapse collapse\">\n                    <ul class=\"nav navbar-nav navbar-right main-nav\">\n                      <li>\n                            <a href=\"#\" class=\"dropdown-toggle settings\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Settings <span class=\"caret\"></span></a>\n                          <ul class=\"dropdown-menu\">\n                                <li><a href=\"#\">{{userName}}</a></li>\n                            <li><a (click)=\"logout()\">Logout</a></li>\n                              <li><a href=\"#\">Something else here</a></li>\n                              <li role=\"separator\" class=\"divider\"></li>\n                              <li><a href=\"#\">Separated link</a></li>\n                          </ul>                \n                      </li>\n                    </ul>\n                  </div>\n                </div>\n              </nav>\n    </header>"

/***/ }),

/***/ "./src/app/layout/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/header/header.component.ts ***!
  \***************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(route, router) {
        this.route = route;
        this.router = router;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.userName = sessionStorage.getItem('hospitalUserName');
    };
    HeaderComponent.prototype.logout = function () {
        sessionStorage.clear();
        this.router.navigate(['/login']);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/layout/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/layout/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout.service.ts":
/*!******************************************!*\
  !*** ./src/app/layout/layout.service.ts ***!
  \******************************************/
/*! exports provided: LayoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return LayoutService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json' })
};
var LayoutService = /** @class */ (function () {
    // url1: string = environment.api.base + environment.api.endPoints.roleModule;
    // url2: string = environment.api.base + environment.api.endPoints.rolemenu1;
    function LayoutService(httpClient) {
        this.httpClient = httpClient;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.base + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endPoints.menus;
    }
    LayoutService.prototype.getMenus = function (link) {
        return this.httpClient.get(this.url + '/' + link, httpOptions);
    };
    LayoutService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LayoutService);
    return LayoutService;
}());



/***/ }),

/***/ "./src/app/material/material.module.ts":
/*!*********************************************!*\
  !*** ./src/app/material/material.module.ts ***!
  \*********************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var modules = [
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCardModule"]
];
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [modules],
            exports: [modules]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    api: {
        // base: 'http://localhost:8080/v1/',
        base: 'http://18.191.136.113:83/v1/',
        endPoints: {
            login: 'login',
            register: 'register',
            account: 'account',
            menu: 'menu',
            role: 'role',
            roleMenu: 'roleMenus',
            menus: 'menuRole',
            insertRoleMenus: 'insertRoleMenus',
            hotel: 'hotel',
            staff: 'staff',
            roomtype: 'roomType',
            room: 'room',
            floor: 'floor',
            booking: 'booking',
            payment: 'payment',
            invoice: 'invoice',
            cleaning: 'cleaning',
            emailtemplate: 'emailTemplate',
            sendmail: 'sendEmail',
            sendsms: 'sendSms'
        }
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ricoh-sp100/hotel/hotel-angular/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map