const create = function (req) {
    let result = {
        status: true,
        error: ''
    };

    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email does not appear to be valid').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        result.error = errors[0].msg;
        result.status = false;
    }

    return result;
};

module.exports = {
    create: create
}